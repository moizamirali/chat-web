module.exports = {
    JWT_SECRET_KEY: 'chatAppAuth',
    JWT_CONFIRM_KEY: 'chatAppConfirm',
    CONFIRM_URL: '',
    PASSRESET_URL: '',
    issuer: "chatApp",

}
