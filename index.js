const express = require('express');
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser');
const request = require('request');
const JWT = require('jsonwebtoken');
const path = require('path');
const http = require('http');
const socketio = require('socket.io');
const formatMessage = require('./utils/messages');
const {
    userJoin,
    getCurrentUser,
    userLeave,
    getRoomUsers
} = require('./utils/users');

const {
    JWT_SECRET_KEY,
    issuer,
} = require('./config/index')

const app = express();
const server = http.createServer(app);
const io = socketio(server);

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.set('views', __dirname);

app.use(cookieParser())

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));


app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});


getUserId = token => {
    let jwtToken = token.split(/\s+/)
    var decodedUserToken = JWT.verify(jwtToken[1], JWT_SECRET_KEY)
    console.log(decodedUserToken)
    return decodedUserToken.subject
}

//create signtoken
signToken = id => {
    return JWT.sign({
        issuer: issuer,
        subject: id,
        issueAt: new Date().getTime(),
    }, JWT_SECRET_KEY, {
        expiresIn: '12h'
    });
}

function checkAuth(token) {
    if (!token) {
        return 0;
    } else {
        var decodedUserToken = JWT.verify(token, JWT_SECRET_KEY)
        return decodedUserToken.subject;
    }
}

const botName = 'ChatApp User';

// Run when client connects
io.on('connection', socket => {
    console.log("New bot connected!")
    socket.on('joinRoom', ({
        username,
        room
    }) => {
        const user = userJoin(socket.id, username, room);

        socket.join(user.room);

        // Welcome current user
        socket.emit('message', formatMessage(botName, 'Welcome to ChatApp!'));

        // Broadcast when a user connects
        socket.broadcast
            .to(user.room)
            .emit(
                'message',
                formatMessage(botName, `${user.username} has joined the chat`)
            );

        // Send users and room info
        io.to(user.room).emit('roomUsers', {
            room: user.room,
            users: getRoomUsers(user.room)
        });
    });

    // Listen for chatMessage
    socket.on('chatMessage', msg => {
        const user = getCurrentUser(socket.id);

        io.to(user.room).emit('message', formatMessage(user.username, msg));
    });

    // Runs when client disconnects
    socket.on('disconnect', () => {
        const user = userLeave(socket.id);

        if (user) {
            io.to(user.room).emit(
                'message',
                formatMessage(botName, `${user.username} has left the chat`)
            );

            // Send users and room info
            io.to(user.room).emit('roomUsers', {
                room: user.room,
                users: getRoomUsers(user.room)
            });
        }
    });
});

app.get('/', (req, res) => {
    try {
        if (checkAuth(req.cookies.secret) !== 0) {
            return res.redirect('/home');
        }
        return res.render(__dirname + '/public/login.html', {
            err: ""
        })
    } catch (e) {
        console.log(e)
        res.render(__dirname + '/public/error.html');
    }
});

app.get('/login', (req, res) => {
    try {
        if (checkAuth(req.cookies.secret) !== 0) {
            return res.redirect('/home');
        }
        return res.render(__dirname + '/public/login.html', {
            err: ""
        })
    } catch (e) {
        console.log(e)
        res.render(__dirname + '/public/error.html');
    }
});

app.get('/register', (req, res) => {
    try {
        if (checkAuth(req.cookies.secret) !== 0) {
            return res.redirect('/home');
        }
        return res.render(__dirname + '/public/register.html', {
            err: ""
        })
    } catch (e) {
        console.log(e)
        res.render(__dirname + '/public/error.html');
    }
});

app.get('/chat', (req, res) => {
    try {
        if (checkAuth(req.cookies.secret) !== 0) {
            return res.render(__dirname + '/public/chat.html');
        }
        return res.render(__dirname + '/public/login.html', {
            err: ""
        })
    } catch (e) {
        console.log(e)
        res.render(__dirname + '/public/error.html');
    }
});

app.post('/register', (req, res) => {
    try {
        var data = {
            "fullName": req.body.fullName,
            "phone": req.body.phone,
            "email": req.body.email,
            "password": req.body.password,
            "confirmPassword": req.body.confirmPassword
        }
        request.post({
            url: 'http://localhost:3010/users/signup',
            headers: {
                "content-type": "application/json",
            },
            body: data,
            json: true
        }, (err, response, body) => {
            console.log(response.body)
            if (response.statusCode === 200) {
                res.cookie('secret', response.body.token);
                return res.redirect('/home');
            } else {
                res.clearCookie('secret');
                return res.render(__dirname + '/public/register.html', {
                    err: err,
                });

            }
        });
    } catch (e) {
        console.log(e)
        res.render(__dirname + '/public/error.html');
    }
});

app.post('/login', async (req, res) => {
    try {
        var data = {
            "email": req.body.email,
            "password": req.body.password
        }
        request.post({
            url: 'http://localhost:3010/users/signin',
            headers: {
                "content-type": "application/json",
            },
            body: data,
            json: true
        }, (err, response, body) => {
            if (response.statusCode === 200) {
                res.cookie('secret', response.body.token);
                return res.redirect('/home');
            } else {
                res.clearCookie('secret');
                return res.render(__dirname + '/public/login.html', {
                    err: "Something went wrong",
                });

            }
        });
    } catch (e) {
        console.log(e)
        res.render(__dirname + '/public/error.html');
    }
});

app.get('/home', async (req, res) => {
    try {
        if (checkAuth(req.cookies.secret) === 0) {
            return res.redirect('/');
        } else {
            request.get({
                url: 'http://localhost:3010/users/all',
                headers: {
                    "content-type": "application/json",
                    'Authorization': `Bearer ${req.cookies.secret}`,
                    'Accept': 'application/json'
                },
                json: true
            }, (err, response, body) => {
                if (response.statusCode === 200) {
                    var data = body.data
                    console.log(data);
                    return res.render(__dirname + '/public/home.html', {
                        data: data,
                        err: null
                    });
                } else {
                    return res.render(__dirname + '/public/home.html', {
                        err: "Something went wrong",
                        data: null,
                    });

                }
                return res.render(__dirname + '/public/home.html', {
                    data: [],
                    err: null
                });
            });
        }
    } catch (e) {
        console.log(e)
        res.render(__dirname + '/public/error.html');
    }
});


app.get('/logout', (req, res) => {
    try {
        res.clearCookie('secret');
        return res.redirect('/');
    } catch (e) {
        console.log(e)
        res.render(__dirname + '/public/error.html');
    }
});



const PORT = 3000;
server.listen(PORT, () => {
    console.log(`Server listen to ${PORT}`);
});